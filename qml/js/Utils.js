// SPDX-FileCopyrightText: © 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

function getCallsign() {
    var s;
    s = String.fromCharCode(97 + Math.round(Math.random() * 25))
      + String.fromCharCode(97 + Math.round(Math.random() * 25))
      + String.fromCharCode(97 + Math.round(Math.random() * 25))
      + Math.round(Math.random() * 9)
      + Math.round(Math.random() * 9)
      + Math.round(Math.random() * 9)
      + Math.round(Math.random() * 9);
    return s.toUpperCase();
}

function parseIsoDatetime(dtstr) {
    var dt = dtstr.split(/[: T-]/).map(parseFloat);
    return new Date(dt[0], dt[1] - 1, dt[2], dt[3] || 0, dt[4] || 0, dt[5] || 0, 0);
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
