// SPDX-FileCopyrightText: © 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

var index = [
 {"filename": "fonts/Ticketing.ttf",			"displayName": "Ticketing",			"smallcaps": false},
 {"filename": "fonts/Data_Control-latin.ttf",	"displayName": "Data Control","smallcaps": false},
 {"filename": "fonts/White_Rabbit.ttf",			"displayName": "White Rabbit",		"smallcaps": false},
 {"filename": "fonts/Techno_LCD.ttf",			"displayName": "Techno LCD",		"smallcaps": true},
 {"filename": "fonts/VT323-Regular.ttf",		"displayName": "VT323",				"smallcaps": true}
];
