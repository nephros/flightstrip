// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Dialog {
  id: dialogpage

  canAccept: false

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  readonly property string defaultPath: StandardPaths.documents
  property string selectedFile: ""
  property string selectedName: ""
  property string selectedUrl: ""
  property var fileData
  property var outData

  states: [
    State { name: "loaded" ;   PropertyChanges { target: dialogpage; canAccept: true} },
    State { name: "selected" ; PropertyChanges { target: button; description: qsTr("Load") } },
    State { name: "failed" }
  ]
  onStateChanged: console.debug("State change :" + state);

  onStatusChanged: {
      if (status == PageStatus.Active && state == "")
          pageStack.push(pickerPage);
  }

  function getData() {
    var fileUrl = dialogpage.selectedUrl;

    var r = new XMLHttpRequest()
    r.open('GET', fileUrl);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.send();

    r.onreadystatechange = function(event) {
      if (r.readyState == XMLHttpRequest.DONE) {
        //var j = JSON.parse(r.response);
        dialogpage.state = "loaded"
        dialogpage.fileData  = r.response;
      }
    }
  }

  onDone: {
    if (result == DialogResult.Accepted) {
        console.debug("Done!");
        outData = fileData;
    }
  }

  Component {
    id: pickerPage
    FilePickerPage {
      nameFilters: [ '*.txt', '*.todo', '*.md', '*.todo.txt' ]
      title: "Select TODO list"
      showSystemFiles: false
      onSelectedContentPropertiesChanged: {
        dialogpage.selectedFile = selectedContentProperties.filePath;
        dialogpage.selectedName = selectedContentProperties.fileName;
        dialogpage.selectedUrl = selectedContentProperties.url;
        dialogpage.state = "selected"
        dialogpage.getData();
      }
    }
  }
  SilicaFlickable {
    id: flick
    contentHeight: col.height + header.height
    anchors.fill: parent

    //DialogHeader {id: header ; width: dialogpage.width; title: qsTr("Load Todo List"); acceptText: qsTr("Place on Board")}
    DialogHeader {id: header ; acceptText: qsTr("Place on Board")}
    Column {
      id: col
      width: parent.width - Theme.horizontalPageMargin
      spacing: Theme.paddingSmall
      anchors.top: header.bottom
      anchors.horizontalCenter: parent.horizontalCenter
      //dialogpageHeader { title: qsTr("Loading Functions") }
      Label {
        width: parent.width
        anchors {
          horizontalCenter: parent.horizontalCenter
          leftMargin: Theme.horizontalPageMargin
          rightMargin: Theme.horizontalPageMargin
        }
        font.pixelSize: Theme.fontSizeSmall
        horizontalAlignment: Text.AlignJustify
        wrapMode: Text.WordWrap
        text: qsTr("Here you can load a Todo list from a file.")
      }
      ValueButton {
        id: button
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        label: qsTr("File name ")
        value: dialogpage.selectedName
        description: qsTr("Click to select")

        onClicked: {
          if ( dialogpage.state != "selected" ) {
            pageStack.push(pickerPage);
          }
        }
      }
      SectionHeader { text: qsTr("File content") }
      TextArea {
        width: parent.width - Theme.paddingLarge * 2
        height: implicitHeight > 0 ? Math.min(implicitHeight, Theme.itemSizeLarge * 6) : Theme.itemSizeLarge * 2
        backgroundStyle: TextEditor.FilledBackground
        background: Component { Rectangle { color: Theme.highlightBackgroundFromColor(Theme.highlightBackgroundColor, Theme.colorScheme); anchors.fill: parent} }
        color: Theme.secondaryColor
        //description: dialogpage.selectedName
        font.pixelSize: Theme.fontSizeSmall
        placeholderText: qsTr("No file loaded")
        readOnly: true
        text: (dialogpage.state == "loaded") ? dialogpage.fileData : ""
        verticalAlignment: TextEdit.AlignVCenter
      }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
