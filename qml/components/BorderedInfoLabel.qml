// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

DetailItem {

    valueFont.family: app.customFont ? app.customFontName : "mono"
    valueFont.capitalization: Font.SmallCaps
    Rectangle {
        anchors.fill:parent
        z: 0
        border.width: 1
        border.color: Theme.secondaryColor
        color: "transparent"
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
