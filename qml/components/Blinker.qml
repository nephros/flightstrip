// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

GlassItem { id: indicator
    property bool busy: false
    property bool checked: false
    //property alias color: backgroundColor
    //backgroundColor: color
    width: Theme.itemSizeExtraSmall /2
    height: Theme.itemSizeExtraSmall /2
    radius: 0.22
    falloffRadius: 0.18

    states: State {
        when: busy
        PropertyChanges { target: indicator; brightness: busyTimer.brightness; dimmed: false; falloffRadius: busyTimer.falloffRadius; opacity: 1.0 }
    }
    Timer {
        id: busyTimer
        property real brightness: Theme.opacityLow
        property real falloffRadius: 0.075
        running: busy && Qt.application.active
        interval: 500
        property int minInt: 400
        property int maxInt: 800
        repeat: true
        onRunningChanged: {
            brightness = checked ? 1.0 : Theme.opacityLow
            falloffRadius = checked ? indicator.defaultFalloffRadius : 0.075
        }
        onTriggered: {
            falloffRadius = falloffRadius === 0.075 ? indicator.defaultFalloffRadius : 0.075
            brightness = brightness == Theme.opacityLow ? 1.0 : Theme.opacityLow
            busyTimer.interval = Math.round(busyTimer.minInt + (Math.random() * (busyTimer.maxInt - busyTimer.minInt + 1)))
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
