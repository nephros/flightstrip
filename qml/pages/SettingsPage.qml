// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../fonts/fonts.js" as Fonts

Page { id: settings

    SilicaFlickable{
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            spacing: Theme.paddingSmall
            bottomPadding: Theme.itemSizeLarge
            width: parent.width - Theme.horizontalPageMargin
            PageHeader{ title:  Qt.application.name + " " + qsTr("Settings", "page title") }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Application")
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: !app.gravity
                automaticCheck: true
                text: qsTr("Reverse Gravity")
                description: qsTr("If disabled, items float to the top instead of being pulled to the bottom.")
                 onClicked: app.gravity = !checked
            }
            TextSwitch{
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: !app.loadfinished
                automaticCheck: true
                text: qsTr("Include Finished when loading")
                description: qsTr("If enabled, loading Todos will include those marked as completed.")
                 onClicked: app.loadfinished = checked
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("User Interface")
            }
            TextSwitch{id: fontsw
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.customFont
                automaticCheck: true
                text: qsTr("Use custom fonts")
                description: qsTr("If enabled, a custom font is used for the FlightStrips.") + " " + qsTr("App restart recommended after changing this.")
                onClicked: app.customFont = checked
            }
            ComboBox {
                enabled: fontsw.checked
                label: qsTr("Fontface")
                description: qsTr("The custom font to use.") + " " + qsTr("App restart recommended after changing this.")
                currentIndex: (app.customFontFace) ?  app.customFontFace : 0
                menu: ContextMenu { Repeater {
                        model: Fonts.index.length
                        delegate: MenuItem { text: Fonts.index[index].displayName }
                }}
                onValueChanged: app.customFontFace = currentIndex
            }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4

